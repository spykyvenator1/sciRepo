# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="A library for linear optimisation"
HOMEPAGE="https://highs.dev/#top"
SRC_URI="https://github.com/ERGO-Code/HiGHS/archive/refs/tags/v${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="dev-build/cmake:="

src_compile(){
	cmake_src_configure
}
